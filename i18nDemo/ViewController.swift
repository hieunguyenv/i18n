//
//  ViewController.swift
//  i18nDemo
//
//  Created by hieu.nguyenv on 8/30/18.
//  Copyright © 2018 Nguyen Van Hieu. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var lblMeasurement: UILabel!
    var supportedLanguages: [String:String] = [:]
    var value = 1.0
    var unit = UnitMass.kilograms
    var locale = Locale.current

    @IBOutlet weak var weightLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        readPlistFileForSupportedLanguages()

        let locale: Locale = self.getLocale(atIndex: 0)

        renderLabel(withValue: self.value, andUnit: self.unit, inLocale: locale)
        let language: String = locale.identifier.components(separatedBy: "_")[0]
        let weightString: String = "Weight".localized(lang: language)
        weightLabel.text = weightString
    }
    
    func readPlistFileForSupportedLanguages() {
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"),
           let infoDict = NSDictionary(contentsOfFile: path){
            supportedLanguages = infoDict.value(forKey: "SupportedLanguages") as! Dictionary
        }
    }

    func getLocale(atIndex index: Int) -> Locale {
        let id = Array(supportedLanguages)[index].key
        return Locale(identifier: id)
    }

    func renderLabel (withValue value: Double, andUnit unit: UnitMass, inLocale locale: Locale) {
        let mass = Measurement(value: value, unit: unit)

        let formatter = MeasurementFormatter()
        formatter.locale = locale

        lblMeasurement.text = formatter.string(from: mass)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.supportedLanguages.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Array(supportedLanguages)[row].value
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let locale: Locale = self.getLocale(atIndex: row)
        renderLabel(withValue: self.value, andUnit: self.unit, inLocale: locale)
        let language: String = locale.identifier.components(separatedBy: "_")[0]
        let weightString: String = "Weight".localized(lang: language)
        weightLabel.text = weightString
    }
}

extension String {
    func localized(lang:String) ->String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }}

